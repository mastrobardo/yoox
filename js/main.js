var yoox = {
    BASE_PATH : "data/",
    CONFIGS :  ["one.json","two.json","three.json","four.json"]
};

yoox.TEST_APP = function(){
    var self = this;
    this.currentlyLoading = 0;
    this.config = yoox.CONFIGS
    this.current = 0;
    console.log("1" + self);

    this.init = function(){
        for(var i = 0, len = self.config.length; i<len; ++i){
           $("#img_container").append("<img src='' id='img_" + i +"'  class='item_img' /> ");
        }
        return self;
    };

    this.renderItem = function(index){
        if(typeof self.config[index] === 'string')
             return self.loadSingle(index).done(function(){ _realRender(index) });
        else return _realRender(index)
    };

    var _realRender = function(index){
       var c = self.config[index].item;
       var images = c.images;
       return $(".item_img").each(function(index, ele){
            $(this).attr("src", images[index]);
       })
    };

    var _next = function(){
        return ++self.currentlyLoading;
    };

    this.loadSingle = function(index){
      return  $.ajax(
                {
                    url: yoox.BASE_PATH + yoox.CONFIGS[index]
                }
            )
            .done(function(data){
                self.config[self.currentlyLoading] = data;
                console.log(self.config);
                _next();
            })
            .error(function(err){
            })
    };

    this.loadAll = function(){
            /// sarebbe meglio fare il loading di tutta la config insieme
    };

    this.submitForm = function(){

        if(!_checkForm()){
            ///TODO: forse meglio una modal con messaggi ?
            alert("CHECK FORM");
            return;
        }

        $.ajax(
              {
                url: "/someurl.php",
                type:'POST',
                data:{
                      name :$("#name").val() ,
                      email :$("#email").val(),
                      tel : (($("#tel").val() != '' ) ? $("#tel").val() : "ND")
                     }
               }
            )
            .done(function(data){
              alert("FORM SUBMITTED");
            })
            .error(function(err){
                alert("FORM ERROR");
            })
    }

    var _checkForm = function(){
        if( $("#name").val() == "" ){
            return false;
        }

        if( $("#email").val() == "" ){
            return false;
        }else{
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return re.test($("#email").val())
        }

        if($("#tel").val() != ''){
            var ret =  /[0-9]|\./;
            return  ret.test($("#tel").val());
        }

        return true;


    }

    this.showNews = function(){
        if(!$("#newscontainer").hasClass('opened')) {
            $( "#newscontainer" ).animate({

                height:"1017px"

            }, 1000, "fast", function() {
                $("#newscontainer").addClass("opened");
            });
        }    else {
            $( "#newscontainer" ).animate({

                height:"386px"

            }, 1000, "fast", function() {
                $("#newscontainer").removeClass("opened");
            });
        }
    }

}